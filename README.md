<div align="center">

# cmd.run/antlr

Containerization for Antlr4 🚀⚡🔥<br>

_Suggestions are always welcome!_

</div>

## Description
_(from the original project website)_

ANTLR (ANother Tool for Language Recognition) is a powerful parser generator for reading, processing, executing, or translating structured text or binary files. It's widely used to build languages, tools, and frameworks. From a grammar, ANTLR generates a parser that can build and walk parse trees.

## Prerequisites

Download and install:

- [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04) 

## What's included in the image?

- [ANTLR 4.9](https://www.antlr.org/)

## What's included in this repo?

- A minimal and clean _Dockerfile_ that defines the installation of above mentioned package
- A _makefile_ that simplifies building and testing if you are utilizing linux
- An example _g4_ (grammar) file for testing

## Getting Started

**To use the image directly**

1. Pull the image from GitLab:
```sh
docker pull registry.gitlab.com/cmd.run/antlr:latest
```
2. To use ANTLR to generate parsers, modify the following command and run:
```sh
docker run --rm -it -v <HOST_DIR>:/workspace \
    registry.gitlab.com/cmd.run/antlr \
    antlr <ANTLR_ARGS>
```

**To build it from scratch**

1. Clone the repository and switch directories:

```sh
git clone https://gitlab.com/cmd.run/antlr.git
cd antlr
```

2. Start the build:
   
```sh
make build
```

## Acknowledgment

All credit goes to [Terence Parr](https://twitter.com/the_antlr_guy) for developing the amazing tool that forms core of this repository and many applications. 

## License

This project is licensed under the MIT License.

```
MIT License

Copyright (c) 2022 machinelearning-one

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

# --------------
# cmd.run/antlr
# --------------

# Set the base image to fetch jar
# --------------------------------
FROM alpine:3.15 AS fetch

# Add utility to fetch jar
# -------------------------
RUN apk add --update --no-cache curl

# Set working directory to an uncluttered place
# ----------------------------------------------
WORKDIR /data

# Fetch the jar
# --------------
RUN curl -O https://www.antlr.org/download/antlr-4.9-complete.jar

# ------------------------------------------------------------------

# Set the base image for actual use
# ----------------------------------
FROM eclipse-temurin:17-jre-alpine

# Copy the jar to local libs
# ---------------------------
COPY --from=fetch /data/antlr-4.9-complete.jar /usr/local/lib/

# Create a script to run the jar
# -------------------------------
COPY --chmod=764 ./antlr /usr/bin/

# Add the jar to the path
# ------------------------
ENV CLASSPATH .:/usr/local/lib/antlr-4.9-complete.jar:$CLASSPATH

# Set working directory to an uncluttered place
# ----------------------------------------------
WORKDIR /workspace

IMAGE=registry.gitlab.com/cmd.run/antlr

build:
	@DOCKER_BUILDKIT=1 docker build -t ${IMAGE} .

test:
	@docker run --rm -it -v ${PWD}/playground:/workspace ${IMAGE} antlr Hello.g4

clean:
	@cd playground && rm -rf *.java *.tokens *.interp